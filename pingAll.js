#!/usr/bin/env node

const EventEmitter		= require("events");
const assert			= require("assert");

const ping				= require("ping");

console.info_			= console.error;

//==============================================================================

const parallelMax		= 254-1+1;

//==============================================================================

function usage(){
	console.info(`node pingAll.js <TARGET_SEGMENT>`);
	console.info();
	console.info("TARGET_SEGMENT: e.g. 192.168.0.0");
	console.info("                     192.168.0.*");
	console.info("                     192.168.0.x");
	console.info("                     192.168.0.xxx");
	console.info("                     \"192.168.0.[100-150]\"");
	console.info("                     \"192.168.0.{100, 101}\"");
	console.info("                     192.168.0");
	console.info("                     192.168.*.*");
	console.info("                     192.168.*.1");
}

//==============================================================================

function hostSortComp(a, b){
	[a, b]		= [a, b].map((ip) => {
					return ip.split(/\./).map(str => parseInt(str));
				});

	for(let i=0; i<Math.min(a.length, b.length); i++){
		if(a[i] != b[i]){
			return a[i]-b[i];
		}
	}

	return 0;
}

//==============================================================================

function segmentParse(targetSegment){
	if(/^\s*((\d+|x{1,3}|\*|\[\s*\d+\s*\-\s*\d+\s*\]|\{\s*\d+\s*(?:,\s*\d+\s*)+\})(\.(\d+|x{1,3}|\*|\[\s*\d+\s*\-\s*\d+\s*\]|\{\s*\d+\s*(?:,\s*\d+\s*)+\})){0,3})\s*$/.test(targetSegment)){
		targetSegment		= RegExp.$1;
	} else {
		throw new Error(`Bad segment: ${targetSegment}`);
	}

	//..........................................................

	targetSegment			= targetSegment.split(/\./);

	for(let i=0; i<4; i++){
		if(targetSegment[i] == null
		|| /x+|\*/.test(targetSegment[i])
		|| (i==3 && /^0$/.test(targetSegment[i]))
		){
			let [start, end]		= i==3 ? [1, 254] : [0, 255];

			targetSegment[i]		=
				new Array(end-start+1).fill(0).map((entry, index, self) => {
					return start + index;
				});
		} else if(/\[\s*(\d+)\s*\-\s*(\d+)\s*\]/.test(targetSegment[i])){
			let [start, end]		= [RegExp.$1, RegExp.$2].map(seg => parseInt(seg)).sort((a, b) => a-b);

			assert(start < end);
			targetSegment[i]		=
				new Array(end-start+1).fill(0).map((entry, index, self) => {
					return start + index;
				});
		} else if(/\{\s*(\d+\s*(?:,\s*\d+\s*)+)\}/.test(targetSegment[i])){
				let segList			= RegExp.$1.split(/\s*,\s*/).map(seg => parseInt(seg)).sort((a, b) => a-b);
				targetSegment[i]	= segList;
		} else {
			assert(/\d+/.test(targetSegment[i]));
			targetSegment[i]		= [targetSegment[i]];
		}
	}

	//..........................................................

	assert.notEqual(targetSegment[0], null);
	let targetHosts			= Array.prototype.concat([], targetSegment[0]);

	for(let i=1; i<targetSegment.length; i++){
		targetHosts		=
			targetHosts.map(host =>
				targetSegment[i].map(seg => `${host}\.${seg}`)
			);
		targetHosts		= Array.prototype.concat.apply(targetHosts[0], targetHosts.slice(1));
	}

	/**
	for(let host of targetHosts){
		console.debug(host);
	}
	/**/

	return targetHosts;
}

//==============================================================================

function createPingChecker(targetSegment, isEventSorted){
	return new (class extends EventEmitter{
		constructor(){
			super();

			this.targetSegment		= targetSegment;
			this.isEventSorted		= isEventSorted==null ? true : isEventSorted;
		}

		//======================================================

		async check(){
			let targetHosts			= segmentParse(this.targetSegment);

			this.activeHosts		= [];

			for(let i=0; i<targetHosts.length; i+=parallelMax){
				let processHosts		= targetHosts.slice(i, i+parallelMax);

				this.emit("start",
					processHosts.length<=1
						? [processHosts[0]]
						: [processHosts[0], processHosts[processHosts.length-1]]
				);

				let activeHosts_seg	=
					await Promise.all(processHosts.map((entry) => {
						return ping.promise.probe(entry).then(
							(res) => {
								//console.debug(`${entry} is ${res.alive ? "alive" : "dead"}`);

								if(!this.isEventSorted){
									if(res.alive){
										this.emit("entry", entry);
									}
								}

								return Promise.resolve(res.alive ? entry : null);
							}
						);
					}));
				activeHosts_seg		= activeHosts_seg.filter(entry => entry!=null);
				activeHosts_seg.sort(hostSortComp);

				Array.prototype.push.apply(this.activeHosts, activeHosts_seg);

				for(let host of activeHosts_seg){
					//console.debug(host);
					if(this.isEventSorted){
						this.emit("entry", host);
					}
				}
			}

			//this.activeHosts must be already sorted.
			//targetHosts is sorted, and each parallel execution result is also.

			//console.debug(this.activeHosts);
			this.emit("end", this.activeHosts);
			return this.activeHosts;
		}
	})();
}

//==============================================================================

(async () => {
	if(process.argv.length != 3){
		usage();
		return;
	}

	//----------------------------------------------------------

	let pingChecker = createPingChecker(process.argv[2], true)
			.on("start", (range) => {
				console.info_(`Checking ${range.join(" - ")} ...`);
			})
			.on("entry", (host) => {
				console.info(host);
			});

	console.info_(`Check segment: ${pingChecker.targetSegment}`);
	await pingChecker.check();
})().catch((err) => {
	console.error(err);
	process.exit(1);
});
