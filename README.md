Ping All
================================================================================

Overview
--------------------------------------------------------------------------------

Send ping to given IP segment to find active devices.

Usage
--------------------------------------------------------------------------------

```bash
node pingAll.js <TARGET_SEGMENT>
```

| Parameter          | Explanation         |
|:-------------------|:--------------------|
| `<TARGET_SEGMENT>` | IP Segment to check |

`<TARGET_SEGMENT>` allows the patterns below:

| Pattern Example        | Target IP                            | Remarks                                        |
|:-----------------------|:-------------------------------------|:-----------------------------------------------|
| `192.168.1.100`        | `192.168.1.100`                      |                                                |
| `192.168.1.0`          | `192.168.1.1`   ～             `254` | The last `0` is handled as wild card           |
| `192.168.1.*`          | `192.168.1.1`   ～             `254` |                                                |
| `192.168.1.x`          | `192.168.1.1`   ～             `254` |                                                |
| `192.168.1.xxx`        | `192.168.1.1`   ～             `254` |                                                |
| `192.168.0.[100-150]`  | `192.168.1.100` ～             `150` | Quote the pattern to pass it correctly         |
| `192.168.0.{100, 101}` | `192.168.1.100`,               `101` | Quote the pattern to pass it correctly         |
| `192.168.0`            | `192.168.1.1`   ～             `254` | Skipped part is handled as wild card           |
| `192.168.*.*`          | `192.168.0.1`   ～ `192.168.255.254` | Every part allows these patterns               |
| `192.168.*.1`          | `192.168.0.1`   ～ `192.168.255.1`   | Other than the last part allows these patterns |

<!-- vim: set ft=markdown et ts=4 sts=4 sw=4: -->
