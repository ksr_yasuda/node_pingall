Change Log
================================================================================

v1.2.1 - 2020-05-27
--------------------------------------------------------------------------------

### Changed

* Change progress log output destination from stdout to stderr

v1.2.0 - 2018-01-15
--------------------------------------------------------------------------------

### Changed

* Refactor
    - Ping checker object creation

v1.1.0 - 2018-01-15
--------------------------------------------------------------------------------

### Changed

* Refactor
    - `ping` library Promise API use
    - Active device handling to use event emittings

v1.0.1 - 2018-01-11
--------------------------------------------------------------------------------

### Changed

* Fix result sorting
* Support invert IP segment range specification (e.g. `[150-100]`)

v1.0.0 - 2018-01-11
--------------------------------------------------------------------------------

### Changed

* Update target IP segment specification
    - wildcard (`*`)
    - range    (e.g. `[100-150]`)
    - list     (e.g. `{100, 101}`)

v0.1.0 - 2018-01-11
--------------------------------------------------------------------------------

### Added

* Release

<!-- vim: set ft=markdown et ts=4 sts=4 sw=4: -->
